转自 [30种EMC标准电路分享，再不收藏就晚了！ (qq.com)](https://mp.weixin.qq.com/s/GiqMEAoSX26NdEBC7sAfXw)

不一定全用，比较重要的地方、又高压的地方就用。



01 **AC24V接口EMC设计标准电路**

![640](assets/640.jpg)

02 **AC110V-220VEMC设计标准电路**

![640(1)](assets/640(1).jpg)

03 **AC380V接口EMC设计标准电路**

![640(32)](assets/640(32).jpg)

04 **AV接口EMC设计标准电路**

![640(33)](assets/640(33).jpg)

05 **CAN接口EMC设计标准电路**

![640(34)](assets/640(34).jpg)

06 **DC12V接口EMC设计标准电路**

![640(35)](assets/640(35).jpg)

07 **DC24V接口EMC设计标准电路**

![640(36)](assets/640(36).jpg)

08 **DC48接口EMC设计标准电路**

![640(37)](assets/640(37).jpg)

09 **DC110V接口EMC设计标准电路**

![640(38)](assets/640(38).jpg)

10 **DVI EMC设计标准电路**

![640(39)](assets/640(39).jpg)

11 **HDMI接口EMC设计标准电路**

![640(40)](assets/640(40).jpg)

12 **LVDS接口EMC设计标准电路**

![640(41)](assets/640(41).jpg)

13 **PS2接口EMC设计标准电路**

![640(42)](assets/640(42).jpg)

14 **RJ11EMC设计标准电路**

![640(43)](assets/640(43).jpg)

15 **RS232 EMC设计标准电路**

![640(44)](assets/640(44).jpg)

16 **RS485EMC设计标准电路**

![640(45)](assets/640(45).jpg)

17 **SCART接口EMC设计标准电路**

![640(46)](assets/640(46).jpg)

18 **s-video接口EMC设计标准电路**

![640(47)](assets/640(47).jpg)

19 **USB DEVICE EMC设计标准电路**

![640(48)](assets/640(48).jpg)

20 **USB2.0接口EMC设计标准电路**

![640(49)](assets/640(49).jpg)

21 **USB3.0接口EMC设计标准电路**

![640(50)](assets/640(50).jpg)

22 **VGA接口EMC设计标准电路**

![640(51)](assets/640(51).jpg)

23 **差分时钟EMC设计标准电路**

![640(52)](assets/640(52).jpg)

24 **耳机接口EMC设计标准电路**

![640(53)](assets/640(53).jpg)

25 **复合视频接口EMC设计标准电路**

![640(54)](assets/640(54).jpg)

26 **汽车零部件电源口EMC标准设计电路**

![640(55)](assets/640(55).jpg)

27 **室内外天馈浪涌设计标准电路**

![640(56)](assets/640(56).jpg)

28 **无源晶振EMC设计标准电路**

![640(57)](assets/640(57).jpg)

29 **有源晶振EMC设计标准电路**

![640(58)](assets/640(58).jpg)

30 **以太网EMC(EMI)设计标准电路**

![640(59)](assets/640(59).jpg)

31 **以太网EMC（浪涌）设计标准电路(差模要求较高方案）**

![640(60)](assets/640(60).jpg)

32 **以太网EMC(浪涌）中心抽头方案（节约空间）**

![640(61)](assets/640(61).jpg)

